package main

import (
	"fmt"

	"gotee/cmd/gotee/hmm"
)

func foo() int64 {
	return 22
}

func main() {
	fmt.Println(foo())
	fmt.Println(hmm.Hmm())
}
